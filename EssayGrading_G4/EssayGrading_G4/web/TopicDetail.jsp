<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>Essay</title>
<!--        <link rel="stylesheet" href="assets/css/essay.css">-->
    </head>
    <body>
        <!-----------------------------------------Navbar---------------------------------------------------------->
        <jsp:include page="components/Header.jsp"></jsp:include>
        <!----------------------------------------Quiz Cards------------------------------------------------------->          
        <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <img src="img/topic/${topic.imageUrl}" alt="Topic Image" class="img-fluid">
                <h1>${topic.topicName}</h1>
                <p>${topic.description}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <form action="TopicDetailController" method="POST">
                    <input type="hidden" name="topicId" value="${topic.topicId}">
                    <textarea name="title" rows="1" class="form-control" placeholder="Write your title here..."></textarea>
                    <div class="form-group">
                        <textarea name="essay" id="essay" rows="10" class="form-control" placeholder="Write your essay here..."></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit Essay</button>
                </form>
            </div>
        </div>
    </div>
        <!---------------------------------------Quiz Cards End---------------------------------------------------->

        <!-------------------------------------------Footer-------------------------------------------------------->
        <div class="container ">
            <footer class="blog-footer">
                <div class="float-left my-2">Copyright � 2024 - <i>Quizz<i></div>
                            <div class="float-right my-2">
                                <a href="#">Back to top</a>
                            </div>
                            </footer>
                            </div>
                            <!----------------------------------------Footer End------------------------------------------------------->

                            <!---------------------------------------My JavaScript----------------------------------------------------->

                        <%if(session.getAttribute("user")!=null)
	{%>
                        <script>
                            document.getElementById("nav2").style.display = "none";
                            document.getElementById("nav3").style.display = "block";
                        </script>
                        <%} 
                        else
                        {
                        %><script>
                            document.getElementById("nav3").style.display = "none";
                            document.getElementById("nav2").style.display = "block";
                        </script>
                        <%}%>

                        <!-- Optional JavaScript -->
                        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
                        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
                        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
                        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
                        </body>
                        </html>