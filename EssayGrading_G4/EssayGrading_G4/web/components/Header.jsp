<%-- 
    Document   : Header
    Created on : Apr 14, 2024, 9:37:22 PM
    Author     : trand
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <header>
                <div class="container">
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark" style="background-color: #e3f2fd;">

                <a class="navbar-brand" href="index.html"><h4><i>AES</i></h4></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">

                    <!--------------------------------------Nav 1---------------------------------------------------->
                    <ul class="navbar-nav mr-auto">

                        <!--------------------------------------Home-------------------------------------------------------->

                        <li class="nav-item">
                            <a class="nav-link" href="Home.jsp">Home</a>
                        </li>

                        <!----------------------------------------Quiz------------------------------------------------------>

                        <li class="nav-item">
                            <a class="nav-link" href="TopicController">Essays</a>
                        </li>

                        <!------------------------------------My Classroom-------------------------------------------------->

                        <li class="nav-item">
                            <a class="nav-link" href="MyEssayController">My Essays</a>
                        </li>

                        <!---------------------------------------About------------------------------------------------------>
                    </ul>
                    <c:if test="${not empty error}">
                        <div id="notification" class="notification">
                            ${error}
                        </div>
                        <script>
                            // JavaScript to display the notification popup
                            document.getElementById("notification").classList.add("show");
                            // Close the notification after 3 seconds (adjust as needed)
                            setTimeout(function () {
                                document.getElementById("notification").classList.remove("show");
                            }, 3000);
                        </script>
                    </c:if>
                    <!----------------------------------------Nav 2---------------------------------------------------->

                    <div class="float-right" id="nav2">
                        <ul class="navbar-nav mr-auto">

                            <!----------------------------------------Login---------------------------------------------------->

                            <li class="nav-item">
                                <button type="button" class="btn btn-secondary mr-2 mb-1" data-toggle="modal" 
                                        data-target="#LoginModal">
                                    Login</button>
                                <!--------------------------------------Login Modal------------------------------------------------>

                                <div class="modal fade" id="LoginModal" tabindex="-1" role="dialog" aria-labelledby="LoginModalLabel"
                                     aria-hidden="true">
                                    <div class="modal-dialog" role="document">

                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="LoginModalLabel">Login</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>

                                            <div class="modal-body">
                                                <form action="signin" method="post">

                                                    <div class="form-group">
                                                        <label for="exampleInputPerson">Login As:</label>
                                                        <select class="form-control" id="exampleInputPerson" name="role">
                                                            <option value="writer">Writer</option>
                                                            <option value="evaluator">Evaluator</option>
                                                            <option value="council" >Council</option>
                                                        </select>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Email address</label>
                                                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                                                               placeholder="Enter email" name="email">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleInputPassword1">Password</label>
                                                        <input type="password" class="form-control" id="exampleInputPassword1" aria-describedby="passwordHelp"
                                                               placeholder="Password" name="password">
                                                    </div>

                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-primary">Login</button>
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                    </div>
                                                </form>
                                            </div>
                                            <button type="button" class="btn btn-secondary"><a href="resetpassword">Forgot Password?</a></button>


                                        </div>
                                    </div>
                                </div>


                                <!-------------------------------------Login Modal End---------------------------------------------> 
                            </li>

                            <!-----------------------------------------Register------------------------------------------------>

                            <li class="nav-item">
                                <button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#RegisterModal">
                                    Register</button>

                                <!--------------------------------------Register Modal---------------------------------------------->
                                <div class="modal fade" id="RegisterModal" tabindex="-1" role="dialog" aria-labelledby="RegisterModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="RegisterModalLabel">Register</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form action="signup" method="post">

                                                    <div class="form-group">
                                                        <label for="exampleInputName">User Name</label>
                                                        <input type="text" class="form-control" id="exampleInputName" aria-describedby="NameHelp" 
                                                               placeholder="Enter Your Name" name="userName">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Email address</label>
                                                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" 
                                                               placeholder="Enter email" name="email">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="exampleInputPassword1">Password</label>
                                                        <input type="password" class="form-control" id="exampleInputPassword1" aria-describedby="passwordHelp" 
                                                               placeholder="Password" name="password">
                                                        <small id="passwordHelp" class="form-text text-muted">Password must be 5 to 20 characters long.</small>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="exampleInputPassword1">Confirm Password</label>
                                                        <input type="password" class="form-control" id="exampleInputPassword1" 
                                                               placeholder="Confirm Password" name="confirmpassword">
                                                    </div>

                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-primary">Register</button>
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div> 

                    <!----------------------------------------End of Nav2----------------------------------------------->  

                    <!-------------------------------------------Nav3--------------------------------------------------->  
                    <div class="float-right" id="nav3">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <form action="profile" method="get">
                                    <button type="submit" value="logout" class="btn btn-secondary mr-2 mb-1" style="font-family: serif;">
                                        <%=session.getAttribute("user")%></button>
                                </form>     
                            </li>
                            
                            <li class="nav-item">
                                    <button type="submit" value="logout" class="btn btn-outline-danger mr-2 mb-1" style="font-family: serif;">
                                        <a href="ChangePassword.jsp">Change Password</a></button>
                            </li>

                            <li class="nav-item">
                                <form action="logout" method="get">
                                    <button type="submit" value="logout" class="btn btn-outline-danger mr-2 mb-1" style="font-family: serif;">
                                        Logout</button>
                                </form>     
                            </li>
                        </ul>
                    </div>      
                    <!----------------------------------------End of Nav3----------------------------------------------->

                </div>
            </nav>
        </div>
                                </header>
    </body>
</html>
