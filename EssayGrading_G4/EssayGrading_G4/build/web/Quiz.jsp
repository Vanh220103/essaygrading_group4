<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>Essay</title>
    </head>
    <body>
        <!-----------------------------------------Navbar---------------------------------------------------------->
        <jsp:include page="components/Header.jsp"></jsp:include>
            <!----------------------------------------Navbar End------------------------------------------------------->

            <!----------------------------------------Quiz Image------------------------------------------------------->
            <div class="container my-3">
                <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="img/logo_quiz1.png" class="d-block w-100" height="500px">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!---------------------------------------Quiz Image End---------------------------------------------------->

        <!----------------------------------------Quiz Cards------------------------------------------------------->
        <div class="container my-3">

            <div class="row pt-3">
                <c:forEach var="u" items="${listTopic}" varStatus="loop">
                <div class="card col-sm" style="border-style: none;">
                    <a href="TopicDetailController?topicId=${u.topicId}" style="text-decoration:none">
                        <img src="img/topic/${u.imageUrl}" class="card-img-top" height="250px" style="border-style:solid;border-color:black;"></a>
                    <div class="card-body">
                        <h5 class="card-title">${u.topicName}</h5>
                        <p class="card-text">${u.description}</p>
                        <a href="TopicDetailController?topicId=${u.topicId}" class="btn btn-primary">Write essay</a>
                    </div>
                </div>
                </c:forEach>
<!--                <div class="card col-sm" style="border-style: none;">
                    <a href="Physics_Exam.jsp" style="text-decoration:none">
                        <img src="img/imagecard5.jpg" class="card-img-top" height="250px" style="border-style:solid;border-color:black;"></a>
                    <div class="card-body">
                        <h5 class="card-title">Physics Quiz</h5>
                        <p class="card-text">Take Mathematics Quiz to test and sharpen your algebra, arithmetic, geometry and 
                            other mathematical concepts.</p>
                        <a href="Physics_Exam.jsp" class="btn btn-primary">Take Quiz</a>
                    </div>
                </div>

                <div class="card col-sm" style="border-style: none;">
                    <a href="Submit_Quiz.jsp" style="text-decoration:none">
                        <img src="img/imagecard7.jpg" class="card-img-top" height="250px" style="border-style:solid;border-color:black;"></a>
                    <div class="card-body">
                        <h5 class="card-title">Submit Quiz Questions</h5>
                        <p class="card-text">Submit some Quiz Questions which will be reviewed by your teachers and added soon to the Question database for Quiz.</p>
                        <a href="Submit_Quiz.jsp" class="btn btn-primary">Submit Questions</a>
                    </div>
                </div>-->

            </div>
        </div>
        <!---------------------------------------Quiz Cards End---------------------------------------------------->

        <!-------------------------------------------Footer-------------------------------------------------------->
        <div class="container ">
            <footer class="blog-footer">
                <div class="float-left my-2">Copyright � 2024 - <i>Quizz<i></div>
                            <div class="float-right my-2">
                                <a href="#">Back to top</a>
                            </div>
                            </footer>
                            </div>
                            <!----------------------------------------Footer End------------------------------------------------------->

                            <!---------------------------------------My JavaScript----------------------------------------------------->

                        <%if(session.getAttribute("user")!=null)
	{%>
                        <script>
                            document.getElementById("nav2").style.display = "none";
                            document.getElementById("nav3").style.display = "block";
                        </script>
                        <%} 
                        else
                        {
                        %><script>
                            document.getElementById("nav3").style.display = "none";
                            document.getElementById("nav2").style.display = "block";
                        </script>
                        <%}%>

                        <!-- Optional JavaScript -->
                        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
                        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
                        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
                        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
                        </body>
                        </html>