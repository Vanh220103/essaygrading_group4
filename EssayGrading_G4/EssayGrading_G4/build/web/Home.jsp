<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <!-- Bootstrap CSS -->
        <!--  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">-->
        <link rel="stylesheet" href="assets/css/Taskbar.css">

        <title>Home - Quizz</title>
        <style>
            /* CSS for notification popup */
            .notification {
                display: none;
                position: fixed;
                top: 20%;
                left: 50%;
                transform: translate(-50%, -50%);
                background-color: #f2f2f2;
                border: 2px solid #e74c3c;
                padding: 20px;
                border-radius: 5px;
                z-index: 1;
            }

            .notification.show {
                display: block;
            }
        </style>
    </head>
    <body>
        <!-----------------------------------------Navbar---------------------------------------------------------->
        <jsp:include page="components/Header.jsp"></jsp:include>
        <!----------------------------------------Navbar End------------------------------------------------------->

        <!-----------------------------------------Crousel -------------------------------------------------------->
        <div class="container my-3">

            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <a href="Quiz.jsp">
                            <img src="img/17-Best-Content-Writing-Services.jpg" class="d-block w-100" alt="Take A Quiz" height="500px" width="200px">
                        </a>
                    </div>
                    <div class="carousel-item">
                        <a href="StudyMaterial.jsp">
                            <img src="assets/img/image2.jpg" class="d-block w-100" alt="Notes And Study Material" height="500px" width="200px">
                        </a>
                    </div>
                    <div class="carousel-item">
                        <a href="Classroom.jsp">
                            <img src="img/Write-that-Essay-Pro-Banner-for-Sourceessay.webp" class="d-block w-100" alt="My Classroom" height="500px" width="200px">
                        </a>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <!---------------------------------------Crousel End------------------------------------------------------->

        <!---------------------------------------- Cards------------------------------------------------------->
        <div class="container my-3">
            <div class="row pt-3">
                <div class="card col-sm" style="border-style: none;">
                    <a href="Quiz.jsp" style="text-decoration:none">
                        <img src="assets/img/imagecard1.webp" class="card-img-top" height="250px" style="border-style:solid;border-color:black;"></a>
                    <div class="card-body">
                        <h5 class="card-title">Take A Quiz</h5>
                        <p class="card-text">Participate in Cool Quizzes to test and sharpen your skills and concepts.</p>
                        <a href="Quiz.jsp" class="btn btn-primary">Go to Quiz Section</a>
                    </div>
                </div>

                <div class="card col-sm" style="border-style: none;">
                    <a href="StudyMaterial.jsp" style="text-decoration:none">
                        <img src="assets/img/imagecard2.jpg" class="card-img-top" height="250px" style="border-style:solid;border-color:black;"></a>
                    <div class="card-body">
                        <h5 class="card-title">Notes and Study Material</h5>
                        <p class="card-text">Share Notes, Study Material,e-books and Tutorials with you peers.</p>
                        <a href="#" class="btn btn-primary">Go To Notes Section</a>
                    </div>
                </div>

                <div class="card col-sm" style="border-style: none;">
                    <a href="Classroom.jsp" style="text-decoration:none">
                        <img src="assets/img/imagecard3.jpg" class="card-img-top" height="250px" style="border-style:solid;border-color:black;"></a>
                    <div class="card-body">
                        <h5 class="card-title">My Classroom</h5>
                        <p class="card-text">Ask and discuss all your doubts and problems with you classmates and teachers.
                        </p>
                        <a href="#" class="btn btn-primary">Go To Classroom Section</a>
                    </div>
                </div>
            </div>
        </div>
        <!--------------------------------------- Cards End---------------------------------------------------->

        <!-------------------------------------------Footer-------------------------------------------------------->
        <div class="container ">
            <footer class="blog-footer">
                <div class="float-left my-2">Copyright � 2024 - <i>Automated Essay System<i></div>
                            <div class="float-right my-2">
                                <a href="#">Back to top</a>
                            </div>
                            </footer>
                            </div>
                            <!----------------------------------------Footer End------------------------------------------------------->

                            <!---------------------------------------My JavaScript----------------------------------------------------->


                            <%if(session.getAttribute("user")!=null)
	{%>
                            <script>
                                document.getElementById("nav2").style.display = "none";
                                document.getElementById("nav3").style.display = "block";
                            </script>
                            <%}
                            else
                            {
                            %><script>
                                document.getElementById("nav3").style.display = "none";
                                document.getElementById("nav2").style.display = "block";
                            </script>
                            <%	}%>


                            <!-- Optional JavaScript -->
                            <!-- jQuery first, then Popper.js, then Bootstrap JS -->
                            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
                            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
                            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
                            </body>
                            </html>