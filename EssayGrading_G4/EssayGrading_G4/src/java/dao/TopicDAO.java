/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import connection.DBContext;
import entity.Topic;

/**
 *
 * @author trand
 */
public class TopicDAO extends DBContext {
    ResultSet rs = null;
    PreparedStatement ps = null;

    public ArrayList<Topic> getAllTopic() {
        ArrayList<Topic> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM essay_grading.topics";
            ps = connection.prepareStatement(sql);
//            ps.setString(1, "%" + search + "%");
//            ps.setString(2, "%" + search + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                Topic a = new Topic(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4));
                list.add(a);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return list;
    }
    
    public Topic getTopicById(String id) {
        try {
            String sql = "SELECT * FROM essay_grading.topics where TopicID =?";
            ps = connection.prepareStatement(sql);
            ps.setString(1, id);
//            ps.setString(2, "%" + search + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                return new Topic(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

}
