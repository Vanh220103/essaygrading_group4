/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import connection.DBContext;
import entity.Essay;
import entity.Topic;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author trand
 */
public class EssayDAO extends DBContext {

    ResultSet rs = null;
    PreparedStatement ps = null;

    public ArrayList<Essay> getAllEssays() {
        ArrayList<Essay> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM essay_grading.topics";
            ps = connection.prepareStatement(sql);
//            ps.setString(1, "%" + search + "%");
//            ps.setString(2, "%" + search + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
//                Topic a = new Topic(rs.getInt(1),
//                        rs.getString(2),
//                        rs.getString(3),
//                        rs.getString(4));
//                list.add(a);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return list;
    }

    public Topic getTopicById(String id) {
        try {
            String sql = "SELECT * FROM essay_grading.topics where TopicID =?";
            ps = connection.prepareStatement(sql);
            ps.setString(1, id);
//            ps.setString(2, "%" + search + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                return new Topic(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    public ArrayList<Essay> getEssayByUser(int userId) {
        ArrayList<Essay> list = new ArrayList<>();
        try {
            String sql = "select e.EssayID,e.Title,e.Content,t.TopicName,e.SubmissionDate from essays e\n"
                    + "inner join topics t on t.TopicID = e.TopicID\n"
                    + "where e.WriterID = ?";
            ps = connection.prepareStatement(sql);
            ps.setInt(1, userId);
            rs = ps.executeQuery();
            while (rs.next()) {
                Essay a = new Essay(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getDate(5));
                list.add(a);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return list;
    }

    public void submitEssay(String title, String essay, int userIdObj, String topicId) {
        try {
            String sql = "INSERT INTO essay_grading.essays (Title, Content,WriterID, TopicID)VALUES (?,?,?,?)";
            ps = connection.prepareStatement(sql);
            ps.setString(1, title);
            ps.setString(2, essay);
            ps.setInt(3, userIdObj);
            ps.setString(4, topicId);
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
