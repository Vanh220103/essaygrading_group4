/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import entity.User;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import connection.DBContext;

public class UserDAO extends DBContext {

    ResultSet rs = null;
    PreparedStatement ps = null;

    public ArrayList<User> getAllUser() {
        ArrayList<User> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM essaygradingdb.user";
            ps = connection.prepareStatement(sql);
//            ps.setString(1, "%" + search + "%");
//            ps.setString(2, "%" + search + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                User a = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5), rs.getInt(6));
                list.add(a);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return list;
    }

    public int checkEmailExist(String email) {
        try {
            String sql = "SELECT UserID FROM essay_grading.users where Email = ?";
            ps = connection.prepareStatement(sql);
            ps.setString(1, email);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public User getUser(String email, String password,int role) {
        try {
            String sql = "SELECT * FROM essaygradingdb.user where Email = ? and Password = ? and RoleID=?";
            ps = connection.prepareStatement(sql);
            ps.setString(1, email);
            ps.setString(2, password);
            ps.setInt(3, role);
            rs = ps.executeQuery();
            while (rs.next()) {
                return new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5), rs.getInt(6));
            }
        } catch (Exception e) {

        }
        return null;
    }

    public void register(String username, String email, String password, String roleName, String status) {
        try {
            String sql = "INSERT INTO essay_grading.users (Username, Password,Role, Email,IsActive)VALUES (?,?,?,?,?)";
            ps = connection.prepareStatement(sql);
            ps.setString(1, username);
            ps.setString(2, password);
            ps.setString(3, roleName);
            ps.setString(4, email);
            ps.setString(5, status);
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void changePassword(String email, String password) {
        try {
            String sql = "Update essay_grading.users set Password = ? where Email = ?";
            ps = connection.prepareStatement(sql);
            ps.setString(1, password);
            ps.setString(2, email);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public void updateProfile(int account_id, String userName, String email, String phone, String address, String specialism) {
        try {
            String sql = "UPDATE [dbo].[account]\n"
                    + "   SET [user_name] = ?\n"
                    + "      ,[email] = ?\n"
                    + "      ,[phone] = ?\n"
                    + "      ,[address] = ?\n"
                    + "      ,[specialism] = ?\n"
                    + " WHERE [account_id] = ?";
            ps = connection.prepareStatement(sql);
            ps.setString(1, userName);
            ps.setString(2, email);
            ps.setString(3, phone);
            ps.setString(4, address);
            ps.setString(5, specialism);
            ps.setInt(6, account_id);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }
    
    public void updateUser(String account_id, String userName, String email, String phone, String address, String specialism,String roleId) {
        try {
            String sql = "UPDATE [dbo].[account]\n"
                    + "   SET [user_name] = ?\n"
                    + "      ,[email] = ?\n"
                    + "      ,[phone] = ?\n"
                    + "      ,[address] = ?\n"
                    + "      ,[specialism] = ?\n"
                    + "      ,[role_id] = ?\n"
                    + " WHERE [account_id] = ?";
            ps = connection.prepareStatement(sql);
            ps.setString(1, userName);
            ps.setString(2, email);
            ps.setString(3, phone);
            ps.setString(4, address);
            ps.setString(5, specialism);
            ps.setString(6, roleId);
            ps.setString(7, account_id);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public User getUserById(int userId) {
        try {
            String sql = "SELECT * FROM essay_grading.users where UserID = ?";
            ps = connection.prepareStatement(sql);
            ps.setInt(1, userId);
            rs = ps.executeQuery();
            while (rs.next()) {
                return new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5), rs.getInt(6));
            }
        } catch (Exception e) {
        }
        return null;
    }

    public int countUser() {
        int count = 0;
        String sql = "SELECT COUNT(*) as 'count'\n"
                + "  FROM account";
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                count = rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return count;
    }

    public int countJob() {
        int count = 0;
        String sql = "SELECT COUNT(*) as 'count'\n"
                + "  FROM job";
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                count = rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return count;
    }

    public int countApply() {
        int count = 0;
        String sql = "SELECT COUNT(*) as 'count'\n"
                + "  FROM apply";
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                count = rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return count;
    }

    public void blockUser(String userID) {
        try {
            String sql = "UPDATE account \n"
                    + "SET status = CASE\n"
                    + "WHEN status = '0' THEN '1'\n"
                    + "WHEN status = '1' THEN '0'\n"
                    + "ELSE status\n"
                    + "end\n"
                    + "where account_id = ?";
            ps = connection.prepareStatement(sql);
            ps.setString(1, userID);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

}
