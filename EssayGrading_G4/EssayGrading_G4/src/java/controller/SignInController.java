
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.UserDAO;
import entity.User;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author HP
 */
@WebServlet(name = "SignInController", urlPatterns = {"/signin"})
public class SignInController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SignInController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SignInController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String role_raw = request.getParameter("role");
        int role = Integer.parseInt(role_raw);

        if (email != null && password != null) {
            UserDAO db = new UserDAO();
            User loggedUser = db.getUser(email, password,role);

            if (loggedUser != null && loggedUser.getIsActive() == 1) {
                session.setAttribute("user", loggedUser);
                session.setAttribute("loggedIn", true);
                if (loggedUser.getRoleID() == 3) {
                    session.setAttribute("userId", loggedUser.getUserID());
                    response.sendRedirect("HomeController");
                } else if (loggedUser.getRoleID() == 2) {
                    session.setAttribute("userId", loggedUser.getUserID());
                    response.sendRedirect("home");
                }else if (loggedUser.getRoleID() == 1) {
                    session.setAttribute("userId", loggedUser.getUserID());
                    response.sendRedirect("StaffDashboard");
                }
                String remember = request.getParameter("remember");
                if (remember != null) {
                    Cookie c_user = new Cookie("user", email);
                    Cookie c_pass = new Cookie("pass", password);
                    c_user.setMaxAge(24 * 36000);
                    c_pass.setMaxAge(24 * 36000);
                    response.addCookie(c_user);
                    response.addCookie(c_pass);
                }

            } else if (loggedUser != null && loggedUser.getIsActive() != 1) {
                request.setAttribute("error", "Your Account have been blocked !!!");
                request.getRequestDispatcher("Home.jsp").forward(request, response);
            } else {
                request.setAttribute("error", "Username or password wrong!!! Please login again");
                request.getRequestDispatcher("Home.jsp").forward(request, response);
            }
        }
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
