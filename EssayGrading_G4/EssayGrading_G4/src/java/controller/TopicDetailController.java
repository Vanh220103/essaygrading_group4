/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.EssayDAO;
import dao.TopicDAO;
import entity.Essay;
import entity.Topic;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;

/**
 *
 * @author trand
 */
@WebServlet(name = "TopicDetailController", urlPatterns = {"/TopicDetailController"})
public class TopicDetailController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet TopicDetailController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet TopicDetailController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        TopicDAO td = new TopicDAO();
        String id = request.getParameter("topicId");
        Integer userIdObj = (Integer) request.getSession().getAttribute("userId");
        if (userIdObj == null) {
            request.setAttribute("error", "You need to login first");
            request.getRequestDispatcher("Home.jsp").forward(request, response);
        }else{
        Topic topic = td.getTopicById(id);
        request.setAttribute("topic", topic);
        request.getRequestDispatcher("TopicDetail.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        EssayDAO td = new EssayDAO();
        String topicId = request.getParameter("topicId");
        String title = request.getParameter("title");
        String essay = request.getParameter("essay");
        Integer userIdObj = (Integer) request.getSession().getAttribute("userId");
        if (userIdObj == null) {
            request.setAttribute("error", "You need to login first");
            request.getRequestDispatcher("Home.jsp").forward(request, response);
        } else {
            td.submitEssay(title,essay,userIdObj,topicId);
            ArrayList<Essay> listEssay = td.getEssayByUser(userIdObj);
            request.setAttribute("listEssay", listEssay);
            request.getRequestDispatcher("MyEssay.jsp").forward(request, response);
        }
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
